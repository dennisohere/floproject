<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Str;
use Modules\Training\Models\Course;
use Modules\Training\Models\Student;

class CourseRegistered extends Notification implements ShouldQueue
{
    use Queueable;
    /**
     * @var Course
     */
    private $course;

    /**
     * Create a new notification instance.
     *
     * @param Course $course
     */
    public function __construct(Course $course)
    {
        //
        $this->course = $course;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        /** @var Student $notifiable */

        $course_registration = $notifiable->getRegistrationForCourse($this->course);

        return (new MailMessage)
            ->subject('Your class details: ' . Str::limit($this->course->title, 15))
            ->greeting('Hello, ' . $notifiable->first_name)
                    ->line('Congratulations for making it into the "' . $this->course->title . '" Online Course')
                    ->line('We are about to embark on a journey that will change your life forever, and I need to get you prepared for it. I have outlined the important details you need to know to help and guide you through the course.')
                    ->line('Please find details below:')
            ->line('Course title: ' . $this->course->title)
            ->line('Login Email: ' . $notifiable->email)
            ->line('Pass-key: ' . $course_registration->passkey)
           ->line('With Love, Florence')
            ;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
