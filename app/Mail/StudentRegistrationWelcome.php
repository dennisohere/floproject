<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Training\Models\Course;
use Modules\Training\Models\Student;

class StudentRegistrationWelcome extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Course
     */
    protected $course;
    /**
     * @var Student
     */
    protected $student;

    /**
     * Create a new message instance.
     *
     * @param Course $course
     * @param Student $student
     */
    public function __construct(Course $course, Student $student)
    {
        //
        $this->course = $course;
        $this->student = $student;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('notifications::emails.student-reg.index', [
            'student' => $this->student,
            'course' => $this->course
        ]);
    }
}
