window.ImageUploader = (function() {
    ImageUploader.imagePath = 'image.png';

    ImageUploader.imageSize = [600, 174];

    function ImageUploader(dialog) {
        this._dialog = dialog;
        this._dialog.addEventListener('cancel', (function(_this) {
            return function() {
                return _this._onCancel();
            };
        })(this));
        this._dialog.addEventListener('imageuploader.cancelupload', (function(_this) {
            return function() {
                return _this._onCancelUpload();
            };
        })(this));
        this._dialog.addEventListener('imageuploader.clear', (function(_this) {
            return function() {
                return _this._onClear();
            };
        })(this));
        this._dialog.addEventListener('imageuploader.fileready', (function(_this) {
            return function(ev) {
                return _this._onFileReady(ev.detail().file);
            };
        })(this));
        this._dialog.addEventListener('imageuploader.mount', (function(_this) {
            return function() {
                return _this._onMount();
            };
        })(this));
        this._dialog.addEventListener('imageuploader.rotateccw', (function(_this) {
            return function() {
                return _this._onRotateCCW();
            };
        })(this));
        this._dialog.addEventListener('imageuploader.rotatecw', (function(_this) {
            return function() {
                return _this._onRotateCW();
            };
        })(this));
        this._dialog.addEventListener('imageuploader.save', (function(_this) {
            return function() {
                return _this._onSave();
            };
        })(this));
        this._dialog.addEventListener('imageuploader.unmount', (function(_this) {
            return function() {
                return _this._onUnmount();
            };
        })(this));
    }

    ImageUploader.prototype._onCancel = function() {};

    ImageUploader.prototype._onCancelUpload = function() {
        clearTimeout(this._uploadingTimeout);
        return this._dialog.state('empty');
    };

    ImageUploader.prototype._onClear = function() {
        return this._dialog.clear();
    };

    ImageUploader.prototype._onFileReady = function(file) {
        // Upload a file to the server
        var formData, dialog = this._dialog;
        // var file = ev.detail().file;

        // Define functions to handle upload progress and completion
        xhrProgress = function (ev) {
            // Set the progress for the upload
            dialog.progress((ev.loaded / ev.total) * 100);
        }

        xhrComplete = function (ev) {
            var response;

            // Check the request is complete
            if (ev.target.readyState != 4) {
                return;
            }

            // Clear the request
            xhr = null;
            xhrProgress = null;
            xhrComplete = null;

            // Handle the result of the upload
            if (parseInt(ev.target.status) == 200) {
                // Unpack the response (from JSON)
                response = JSON.parse(ev.target.responseText);

                // Store the image details
                image = {
                    size: response.size,
                    url: response.url
                };

                // Populate the dialog
                dialog.populate(image.url, image.size);

            } else {
                // The request failed, notify the user
                new ContentTools.FlashUI('no');
            }
        }

        // Set the dialog state to uploading and reset the progress bar to 0
        dialog.state('uploading');
        dialog.progress(0);

        // Build the form data to post to the server
        formData = new FormData();
        formData.append('image', file);
        formData.append('_token', document.head.querySelector('meta[name="csrf-token"]').content);

        // Make the request
        xhr = new XMLHttpRequest();
        xhr.upload.addEventListener('progress', xhrProgress);
        xhr.addEventListener('readystatechange', xhrComplete);
        xhr.open('POST', '/backend/pages/upload-image', true);
        xhr.send(formData);
    };

    ImageUploader.prototype._onMount = function() {};

    ImageUploader.prototype._onRotateCCW = function() {
        var clearBusy;
        this._dialog.busy(true);
        clearBusy = (function(_this) {
            return function() {
                return _this._dialog.busy(false);
            };
        })(this);
        return setTimeout(clearBusy, 1500);
    };

    ImageUploader.prototype._onRotateCW = function() {
        var clearBusy;
        this._dialog.busy(true);
        clearBusy = (function(_this) {
            return function() {
                return _this._dialog.busy(false);
            };
        })(this);
        return setTimeout(clearBusy, 1500);
    };

    ImageUploader.prototype._onSave = function() {
        var crop, cropRegion, formData, dialog = this._dialog;

        // Define a function to handle the request completion
        xhrComplete = function (ev) {
            // Check the request is complete
            if (ev.target.readyState !== 4) {
                return;
            }

            // Clear the request
            xhr = null
            xhrComplete = null

            // Free the dialog from its busy state
            dialog.busy(false);

            // Handle the result of the rotation
            if (parseInt(ev.target.status) === 200) {
                // Unpack the response (from JSON)
                var response = JSON.parse(ev.target.responseText);

                // Trigger the save event against the dialog with details of the
                // image to be inserted.
                dialog.save(
                    response.url,
                    600,
                    {
                        'alt': response.alt,
                        'data-ce-max-width': 600 // response.size[0]
                    });

            } else {
                // The request failed, notify the user
                new ContentTools.FlashUI('no');
            }
        }

        // Set the dialog to busy while the rotate is performed
        dialog.busy(true);

        // Build the form data to post to the server
        formData = new FormData();
        formData.append('url', image.url);
        formData.append('_token', document.head.querySelector('meta[name="csrf-token"]').content);


        // Set the width of the image when it's inserted, this is a default
        // the user will be able to resize the image afterwards.
        formData.append('width', 600);

        // Check if a crop region has been defined by the user
        if (dialog.cropRegion()) {
            formData.append('crop', dialog.cropRegion());
        }

        // Make the request
        xhr = new XMLHttpRequest();
        xhr.addEventListener('readystatechange', xhrComplete);
        xhr.open('POST', '/backend/pages/insert-image', true);
        xhr.send(formData);
    };

    ImageUploader.prototype._onUnmount = function() {};

    ImageUploader.createImageUploader = function(dialog) {
        return new ImageUploader(dialog);
    };

    return ImageUploader;

})();

window.addEventListener('load', function() {
    var FIXTURE_TOOLS, IMAGE_FIXTURE_TOOLS, LINK_FIXTURE_TOOLS, editor, req;
    ContentTools.IMAGE_UPLOADER = ImageUploader.createImageUploader;

    editor = ContentTools.EditorApp.get();
    // editor.init('*[data-editable]', 'data-name');
    editor.init('[data-editable], [data-fixture]', 'data-name');

    editor.addEventListener('saved', function (ev) {
        var name, payload, regions, xhr;

        // Check that something changed
        regions = ev.detail().regions;
        if (Object.keys(regions).length == 0) {
            return;
        }

        // Set the editor as busy while we save our changes
        this.busy(true);

        // Collect the contents of each region into a FormData instance
        payload = new FormData();
        payload.append('_token', document.head.querySelector('meta[name="csrf-token"]').content);
        // payload.append('images', JSON.stringify(getImages()));
        // payload.append('regions', JSON.stringify(regions));
        for (name in regions) {
            if (regions.hasOwnProperty(name)) {
                payload.append(name, regions[name]);
            }
        }

        // Send the update content to the server to be saved
        function onStateChange(ev) {
            // Check if the request is finished
            if (ev.target.readyState == 4) {
                editor.busy(false);
                if (ev.target.status == '200') {
                    // Save was successful, notify the user with a flash
                    new ContentTools.FlashUI('ok');
                } else {
                    // Save failed, notify the user with a flash
                    new ContentTools.FlashUI('no');
                }
            }
        };

        xhr = new XMLHttpRequest();
        xhr.addEventListener('readystatechange', onStateChange);
        xhr.open('POST', window.location.href);
        xhr.send(payload);
    });

    FIXTURE_TOOLS = [['undo', 'redo', 'remove']];
    IMAGE_FIXTURE_TOOLS = [['undo', 'redo', 'image']];
    LINK_FIXTURE_TOOLS = [['undo', 'redo', 'link']];
    ContentEdit.Root.get().bind('focus', function(element) {
        var tools;
        if (element.isFixed()) {
            if (element.type() === 'ImageFixture') {
                tools = IMAGE_FIXTURE_TOOLS;
            } else if (element.tagName() === 'a') {
                tools = LINK_FIXTURE_TOOLS;
            } else {
                tools = FIXTURE_TOOLS;
            }
        } else {
            tools = ContentTools.DEFAULT_TOOLS;
        }
        if (editor.toolbox().tools() !== tools) {
            return editor.toolbox().tools(tools);
        }
    });

    function getImages() {
        // Return an object containing image URLs and widths for all regions
        var descendants, i, images;

        images = {};
        for (name in editor.regions()) {
            // Search each region for images
            descendants = editor.regions()[name].descendants();
            for (i = 0; i < descendants.length; i++) {
                // Filter out elements that are not images
                if (descendants[i].type() !== 'Image') {
                    continue;
                }
                images[descendants[i].attr('src')] = descendants[i].size()[0];
            }
        }

        console.log('images', images);
        console.log('regions', editor.regions());

        return images;
    }
});
