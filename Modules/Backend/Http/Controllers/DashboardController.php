<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function dashboard()
    {
        return view('backend::dashboard.dashboard');
    }
}
