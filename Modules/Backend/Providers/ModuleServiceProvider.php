<?php

namespace Modules\Backend\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('backend', 'Resources/Lang', 'app'), 'backend');
        $this->loadViewsFrom(module_path('backend', 'Resources/Views', 'app'), 'backend');
        $this->loadMigrationsFrom(module_path('backend', 'Database/Migrations', 'app'), 'backend');
        $this->loadConfigsFrom(module_path('backend', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('backend', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
