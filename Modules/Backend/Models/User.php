<?php

namespace Modules\Backend\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Modules\System\Traits\HasMetadataAttribute;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasMetadataAttribute, HasRoles;

    protected $guard_name = 'web';

    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function updateLastLogin()
    {
        $this->update([
            'last_login' => now()
        ]);

        return $this;
    }

    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'activated_at' => 'datetime',
        'last_login' => 'datetime',
        'activated' => 'boolean',
    ];

    public function updateAccessToken()
    {
        $token = Str::random(60);

        $this->forceFill([
            'api_token' => hash('sha256', $token),
        ])->save();

        return $token;
    }

    public function changeUserPassword($current_password, $new_password)
    {
        if(password_verify($current_password, $this->password)){
            $this->forceFill([
                'password' => bcrypt($new_password)
            ]);
            $this->save();

            return $this;
        }

        return false;
    }
}
