<!-- Navigation -->
<ul class="navbar-nav">
    <li class="nav-item active" >
        <a class=" nav-link active " href="/backend/dashboard"> <i class="ni ni-tv-2 text-primary"></i> Dashboard
        </a>
    </li>
</ul>
<!-- Divider -->
<hr class="my-3">
<!-- Heading -->
<h6 class="navbar-heading text-muted">Manage Data</h6>
<!-- Navigation -->
<ul class="navbar-nav mb-md-3">
    <li class="nav-item">
        <a class="nav-link" href="{{route('backend.training.courses.index')}}">
            <i class="ni ni-books"></i> Courses
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{route('backend.training.students.index')}}">
            <i class="ni ni-circle-08"></i> Students
        </a>
    </li>

</ul>