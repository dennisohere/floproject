<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::prefix('backend/training')->name('backend.training.')->middleware('auth')->namespace('Backend')->group(function () {

    Route::prefix('courses')->name('courses.')->group(function(){
        Route::get('index', 'CoursesController@index')->name('index');
        Route::post('save', 'CoursesController@save')->name('save');

        Route::post('{course}/modules/save', 'CoursesController@saveCourseModule')->name('modules.save');
        Route::get('{course}/modules/{material}/delete', 'CoursesController@deleteCourseModule')->name('modules.delete');
    });

    Route::prefix('students')->name('students.')->group(function(){
        Route::get('index', 'StudentsController@index')->name('index');
//        Route::post('save', 'CoursesController@save')->name('save');
    });

});
//

Route::prefix('training')->name('training.')->group(function () {

    Route::prefix('payments')->name('payments.')->namespace('Backend')->group(function(){
        Route::post('paystack/webhook', 'PaymentController@paystack_webhook')->name('paystack.webhook');
    });

    Route::prefix('course')->name('course.')->group(function(){
        Route::get('{course}/details', 'TrainingController@details')->name('details');
        Route::post('{course}/authenticate', 'TrainingController@authenticate')->name('authenticate');
        Route::get('{course}/authenticate', function(\Modules\Training\Models\Course $course){
            return redirect()->route('training.course.details', ['course' => $course->slug]);
        });
        Route::get('{course}/start', 'TrainingController@start')->name('start');
        Route::get('{material}/watch', 'TrainingController@watch_course_video')->name('material.watch');
    });
});