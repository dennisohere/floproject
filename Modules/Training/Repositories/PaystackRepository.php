<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 8/21/2019
 * Time: 7:26 AM
 */

namespace Modules\Training\Repositories;


use Illuminate\Support\Facades\Storage;
use Modules\System\Traits\SystemRepositoryTrait;

class PaystackRepository
{

    use SystemRepositoryTrait;

    public function logPayment(array $data)
    {
        $newJsonString = json_encode($data, JSON_PRETTY_PRINT);

        $path = 'paystack_logs/' . date('Y-m-d') . '/' . time();

        Storage::disk('local')->put($path . '.json', stripslashes($newJsonString));

    }

}