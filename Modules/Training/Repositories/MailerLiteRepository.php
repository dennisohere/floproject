<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 8/21/2019
 * Time: 9:39 PM
 */

namespace Modules\Training\Repositories;


use MailerLiteApi\MailerLite;
use Modules\System\Traits\SystemRepositoryTrait;
use Modules\Training\Models\Student;

class MailerLiteRepository
{

    use SystemRepositoryTrait;

    /**
     * @var MailerLite
     */
    private $mailerliteClient;

    /**
     * MailerLiteRepository constructor.
     * @throws \MailerLiteApi\Exceptions\MailerLiteSdkException
     */
    public function __construct()
    {
       $this->mailerliteClient  = new MailerLite(config('training_module.mailer_lite.api_key'));
    }


    public function subscribeStudent(Student $student)
    {
        $subscriber = [
            'email' => $student->email,
            'fields' => [
                'name' => $student->first_name,
                'last_name' => $student->last_name,
//                'company' => 'John Doe Co.'
            ]
        ];

        $response = $this->mailerliteClient->groups()
            ->addSubscriber(config('training_module.mailer_lite.groups.flo_igboayaka'), $subscriber);
    }

//    /**
//     * @param $campaign_id
//     * @throws \Exception
//     */
//    public function subscribeToCampaign($campaign_id)
//    {
//        $campaign = $this->mailerliteClient->campaigns()->find($campaign_id);
//
//
//    }
//
//    /**
//     * @throws \Exception
//     */
//    public function subscribeTo_5_Secrets_To_Attract_()
//    {
//        $this->subscribeToCampaign(18215716);
//    }
}