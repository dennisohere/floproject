<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 8/21/2019
 * Time: 7:34 AM
 */

namespace Modules\Training\Repositories;


use Illuminate\Support\Str;
use Modules\System\Traits\SystemRepositoryTrait;
use Modules\Training\Events\StudentHasEnrolledInCourse;
use Modules\Training\Models\Course;

class CourseRepository
{
    use SystemRepositoryTrait;
    /**
     * @var Course
     */
    private $course;


    /**
     * CourseRepository constructor.
     * @param Course $course
     */
    public function __construct(Course $course)
    {
        $this->course = $course;
    }

    /**
     * @param $url
     * @return Course
     */
    public function getCourseFromPaystackPaymentURL($url)
    {
        return $this->course->where('paystack_payment_url', $url)->first();
    }

    public function registerNewStudentOnCourse(Course $course, array $data)
    {
        // todo: create student
        $student = StudentRepository::init()->saveStudent($data);

        $course->registrations()->create([
            'student_id' => $student->id,
            'passkey' => $this->generatePasskey()
        ]);

        event(new StudentHasEnrolledInCourse($course, $student));
    }

    private function generatePasskey()
    {
        return Str::random(9);
    }

    public function getAllCourses()
    {
        return $this->course->orderBy('title', 'asc')->latest()->get();
    }

    public function saveCourse(array $payload)
    {
        $edit = !!isset($payload['id']);

        $course = $edit ? $this->getCourseById($payload['id']) : $this->course->newInstance();

        $course->fill([
            'title' => $payload['title'],
            'description' => $payload['description'],
            'cost' => $payload['cost'] ?? 0,
            'expires_at' => $payload['expiry'],
            'paystack_payment_url' => $payload['paystack_payment_url'],
//            'resource_link' => $payload['resource'],
        ]);

        if(!$edit){
            $course->fill([
               'code' => $this->generateCourseCode(),
                'slug' => $this->generateSlug($payload['title']),
            ]);
        }

        $course->save();

        return $course;
    }

    /**
     * @param $id
     * @return Course | null
     */
    public function getCourseById($id)
    {
        return $this->course->find($id);
    }

    private function generateSlug($title)
    {
        $slug = Str::slug($title);

        while ($exits = $this->getCourseBySlug($slug)){
            $slug = Str::slug($title . rand(10,50));
        }

        return $slug;
    }

    /**
     * @param $slug
     * @return Course
     */
    public function getCourseBySlug($slug)
    {
        return $this->course->where('slug', $slug)->first();
    }

    public function generateCourseCode()
    {
        return Str::random(9);
    }
}