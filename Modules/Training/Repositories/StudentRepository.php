<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 8/21/2019
 * Time: 7:43 AM
 */

namespace Modules\Training\Repositories;


use Modules\System\Traits\SystemRepositoryTrait;
use Modules\Training\Models\Student;

class StudentRepository
{

    use SystemRepositoryTrait;
    /**
     * @var Student
     */
    private $student;


    /**
     * StudentRepository constructor.
     * @param Student $student
     */
    public function __construct(Student $student)
    {
        $this->student = $student;
    }


    /**
     * @param array $payload
     * @return Student
     */
    public function saveStudent(array $payload)
    {
        $edit = !!isset($payload['id']);

        $student = $this->getStudentByEmail($payload['email']);

        if(!$student) $student = $edit ? $this->getStudentById($payload['id']) : $this->student->newInstance();

        $student->fill($payload);

        $student->save();

        return $student;
    }

    /**
     * @param $id
     * @return Student | null
     */
    public function getStudentById($id)
    {
        return $this->student->find($id);
    }

    /**
     * @param $email
     * @return Student | null
     */
    public function getStudentByEmail($email)
    {
        return $this->student->where('email', $email)->first();
    }

    public function getAllStudents()
    {
        return $this->student->all();
    }
}