@extends('backend::layout.base')


@section('base')

    <div class="main-content bg-lighter p-4">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    {!! $material->video_embed_code !!}
                </div>
            </div>
        </div>

    </div>

@endsection