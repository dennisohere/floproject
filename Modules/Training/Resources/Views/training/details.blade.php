@extends('backend::layout.base')


@section('base')

    <div class="main-content bg-lighter">
        <!-- Navbar -->
        <nav class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-light">
            <div class="container px-4">
                <a class="navbar-brand" href="">
                    Course Details
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbar-collapse-main" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbar-collapse-main">

                    <!-- Navbar items -->
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            {{--<a class="nav-link nav-link-icon" href="/">
                                <i class="ni ni-planet"></i>
                                <span class="nav-link-inner--text">Back to main site</span>
                            </a>--}}
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Header -->
        <div class="header py-7 py-lg-8">
            <div class="container">
                <div class="header-body text-center mb-7">
                    <div class="row justify-content-center">
                        <div class="col-lg-5 col-md-6">
                            <h1 class="text-black-50">
                                {{$course->title}}
                            </h1>
                            <p class="text-lead text-info">
                                {{$course->description}}
                            </p>
                            @include('flash::message')

                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- Page content -->
        <div class="container mt--200 pb-5">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7">
                    <div class="card bg-white shadow border-0">
                        @if(!$course->hasExpired() || (auth()->user() && auth()->user()->hasRole([config('backend_module.roles.dev'),config('backend_module.roles.super_admin')])))
                        <div class="card-body px-lg-5 pb-lg-5">
                            <form role="form"
                                  action="{{route('training.course.authenticate', ['course' => $course->slug])}}"
                                  method="post">
                                @csrf
                                <div class="form-group ">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                        </div>
                                        <input class="form-control" name="email" placeholder="Email" type="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>
                                        <input class="form-control" name="passkey" placeholder="Pass Key" type="password">
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary my-4">Start Course</button>
                                </div>
                            </form>
                        </div>
                        @else
                            <div class="card-body px-lg-5 pb-lg-5">
                                <p class="text-warning text-center">
                                    This course no longer available!
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        {{--<footer class="py-5">
            <div class="container">
                <div class="row align-items-center justify-content-xl-between">
                    <div class="col-xl-6">
                        <div class="copyright text-center text-xl-left text-muted">
                            © 2018 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                            <li class="nav-item">
                                <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
                            </li>
                            <li class="nav-item">
                                <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
                            </li>
                            <li class="nav-item">
                                <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
                            </li>
                            <li class="nav-item">
                                <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>--}}
    </div>

@endsection