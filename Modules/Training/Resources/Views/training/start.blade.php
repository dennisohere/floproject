@extends('backend::layout.base')

@section('base')

    <div class="main-content bg-lighter row p-4">
        <div class="col-sm-3">
            <h3 class="heading-title">
                {{$course->title}}
            </h3>
            <div>
                <p>{{$course->description}}</p>
            </div>
        </div>

        <div class="col-sm-9 bg-white" style="height: 600px; overflow-y: auto">
            <div class="container">
                <div class="table-responsive py-5">
                    <!-- Projects table -->
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Title</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($course->courseMaterials as $courseMaterial)
                            <tr>
                                <td>{{$loop->index + 1}}</td>
                                <th scope="row">
                                    {{$courseMaterial->title}}
                                </th>

                                <td>
                                    <a href="{{route('training.course.material.watch', ['material' => $courseMaterial->id])}}"
                                       onclick="var popup_window = window.open(this.href, 'mywin','location=0,toolbar=0,menubar=0,scrollbars=1,height=600,width=1000');
                           return false;"
                                       class="btn btn-info btn-sm">
                                        Watch Video
                                    </a>
                                    @if($courseMaterial->download_link)
                                    <a href="{{$courseMaterial->download_link}}" target="_blank"
                                       class="btn btn-secondary btn-sm">
                                        Download Resource
                                    </a>
                                    @endif
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

@endsection