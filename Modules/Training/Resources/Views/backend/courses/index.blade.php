@extends('backend::layout.app')

@section('app')

    <div class="row">

        <div class="col-sm-12">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Courses</h3>
                        </div>
                        <div class="col text-right">
                            <button class="btn btn-sm btn-primary"
                                    data-target="#modalNew" data-toggle="modal">
                                Add Course
                            </button>
                        </div>
                    </div>
                </div>

                @if($courses->count() > 0)

                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Title</th>
                                <th scope="col">Cost</th>
                                <th scope="col">Total <br>Registrations</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($courses as $course)
                                <tr>
                                    <th scope="row">
                                        {{$course->title}}
                                    </th>
                                    <td>
                                        {{$course->cost}}
                                    </td>
                                    <td>
                                        {{$course->courseMaterials()->count()}}
                                    </td>
                                    <td>
                                        <a href="?edit={{$course->slug}}" class="btn btn-sm btn-secondary">
                                            <i class="ni ni-ruler-pencil"></i>
                                            Edit
                                        </a>
                                        <a href="?course={{$course->slug}}&modal=module-list" class="btn btn-sm btn-success">
                                            <i class="ni ni-ruler-pencil"></i>
                                            Modules
                                        </a>
                                        <a href="{{route('training.course.details',['course' => $course->slug])}}"
                                           target="_blank"
                                           class="btn btn-sm btn-outline-secondary">
                                            <i class="ni ni-camera-compact"></i>
                                            Preview
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                @else

                    <div class="card-body">
                        <p class="card-title text-center text-warning">No Data Available.</p>
                    </div>

                @endif

            </div>
        </div>

    </div>


    <div class="modal fade stick-up" id="modalNew" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Course...</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="{{route('backend.training.courses.save')}}" method="post" class=""
                      enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="title" class="form-control-label">
                                Title
                            </label>
                            <input type="text" class="form-control form-control-sm form-control-alternative" id="title"
                                   placeholder="" name="title" >
                        </div>

                        <div class="form-group">
                            <label for="expiry" class="form-control-label">
                                Expiry
                            </label>
                            <div class="input-group input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                </div>
                                <input class="form-control datepicker" value="{{date('Y-m-d')}}"
                                       data-date-format="yyyy-mm-d"
                                       placeholder="Select date" type="text" name="expiry">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="cost" class="form-control-label">
                                Cost
                            </label>
                            <input type="number" class="form-control form-control-sm form-control-alternative" id="cost"
                                   placeholder="Default: 0 (FREE)" name="cost" >
                        </div>

                        {{--<div class="form-group">
                            <label for="resource" class="form-control-label">
                                Resource URL
                            </label>
                            <input type="text" class="form-control form-control-sm form-control-alternative" id="resource"
                                   placeholder="Link to course resource" name="resource" >
                        </div>--}}

                        <div class="form-group">
                            <label for="path" class="form-control-label">
                                Paystack Form URL
                            </label>
                            <input type="text" class="form-control form-control-sm form-control-alternative" id="path"
                                   placeholder="Paystack Payment URL" name="paystack_payment_url" >
                        </div>


                        <div class="form-group">
                            <label for="description" class="form-control-label">
                                Description
                            </label>
                            <textarea name="description" id="description" cols="" rows="4"
                                      class="form-control form-control-sm form-control-alternative"></textarea>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-outline-primary btn-sm">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

    @if(isset($edit) && $edit)

        <div class="modal fade stick-up" id="modalEdit" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit: {{$edit->title}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <form action="{{route('backend.training.courses.save')}}" method="post" class=""
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{$edit->id}}">
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="title" class="form-control-label">
                                    Title
                                </label>
                                <input type="text" class="form-control form-control-sm form-control-alternative" id="title"
                                       placeholder="" name="title" value="{{$edit->title}}">
                            </div>

                            <div class="form-group">
                                <label for="expiry" class="form-control-label">
                                    Expiry
                                </label>
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                    </div>
                                    <input class="form-control datepicker"
                                           data-date-format="yyyy-mm-d"
                                           value="{{$edit->expires_at ? $edit->expires_at->format('Y-m-d') : null}}"
                                           placeholder="Select date" type="text" name="expiry">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="cost" class="form-control-label">
                                    Cost
                                </label>
                                <input type="number" class="form-control form-control-sm form-control-alternative" id="cost"
                                       placeholder="Default: 0 (FREE)" name="cost" value="{{$edit->cost}}">
                            </div>

                           {{-- <div class="form-group">
                                <label for="resource" class="form-control-label">
                                    Resource URL
                                </label>
                                <input type="text" class="form-control form-control-sm form-control-alternative" id="resource"
                                       placeholder="Link to course resource" name="resource" value="{{$edit->resource_link}}">
                            </div>--}}

                            <div class="form-group">
                                <label for="path" class="form-control-label">
                                    Paystack Form URL
                                </label>
                                <input type="text" class="form-control form-control-sm form-control-alternative" id="path"
                                       placeholder="Paystack Payment URL" name="paystack_payment_url"
                                       value="{{$edit->paystack_payment_url}}">
                            </div>


                            <div class="form-group">
                                <label for="description" class="form-control-label">
                                    Description
                                </label>
                                <textarea name="description" id="description" cols="" rows="4"
                                          class="form-control form-control-sm form-control-alternative">{{$edit->description}}</textarea>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-outline-primary btn-sm">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    @endif


    @if(request()->get('modal') == 'module-list')

    <div class="modal fade stick-up" id="module-list" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">
                        <small>{{$course->title}}:</small>
                        <br>
                        Course Modules
                    </h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    @if($course->courseMaterials->count() > 0)

                        <div class="table-responsive">
                            <!-- Projects table -->
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col">Title</th>
                                    <th scope="col">Video Embed Code</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($course->courseMaterials as $courseMaterial)
                                    <tr>
                                        <th scope="row">
                                            {{$courseMaterial->title}}
                                        </th>
                                        <td>
                                            {{$courseMaterial->video_embed_code}}
                                        </td>
                                        <td>
                                            <a href="{{route('backend.training.courses.index', ['course'=>$course->slug, 'modal' => 'module-list', 'material-edit'=>$courseMaterial->id])}}"
                                               class="btn btn-sm btn-secondary">Edit</a>

                                            <a href="{{route('backend.training.courses.modules.delete', ['course'=>$course->slug, 'material' => $courseMaterial->id])}}"
                                               class="btn btn-sm btn-danger">Delete</a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    @else

                        <div class="card-body">
                            <p class="card-title text-center text-warning">No Data Available.</p>
                        </div>

                    @endif

                </div>

                <div class="modal-footer bg-light">
                    <form class="form-inline" method="post"
                          action="{{route('backend.training.courses.modules.save', ['course' => $course->slug])}}">
                        @csrf
                        @if(isset($course_material_edit) && $course_material_edit)
                            <input type="hidden" name="id" value="{{$course_material_edit->id}}">
                        @endif
                        <label class="sr-only" for="module_title">Title:</label>
                        <input type="text" class="form-control form-control-sm form-control-alternative mb-2 mr-sm-2"
                               id="module_title" name="title"
                               value="{{isset($course_material_edit) ? $course_material_edit->title : ''}}"
                               placeholder="Title of Module">

                        <label class="sr-only" for="embed_code">Module Video Embed Code:</label>
                        <input type="text" class="form-control form-control-sm form-control-alternative mb-2 mr-sm-2"
                               id="embed_code" name="embed_code"
                               value="{{isset($course_material_edit) ? $course_material_edit->video_embed_code : ''}}"
                               placeholder="Embed code">

                        <label class="sr-only" for="embed_code">Resource Download URL:</label>
                        <input type="text" class="form-control form-control-sm form-control-alternative mb-2 mr-sm-2"
                               id="embed_code" name="download_link"
                               value="{{isset($course_material_edit) ? $course_material_edit->resource_url : ''}}"
                               placeholder="Resource Download URL">

                        <button type="submit" class="btn btn-primary mb-2 btn-sm">
                            {{isset($course_material_edit) ? 'Edit Module...' : 'Add Module'}}
                        </button>
                    </form>
                </div>
            </div>
        </div>

    </div>

    @endif
@endsection


@push('scripts')

    @if(request()->get('modal') == 'module-list')

        <script>
            $('#module-list').modal('show');

            $('#module-list').on('hidden.bs.modal', function () {
                window.location.href = window.location.origin + window.location.pathname;
            })
        </script>

    @endif

@endpush