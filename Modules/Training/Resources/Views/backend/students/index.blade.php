@extends('backend::layout.app')

@section('app')

    <div class="row">

        <div class="col-sm-12">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Students</h3>
                        </div>
                        <div class="col text-right">

                        </div>
                    </div>
                </div>

                @if($students->count() > 0)

                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($students as $student)
                                <tr>
                                    <th scope="row">
                                        {{$student->getFullName()}}
                                    </th>
                                    <td>
                                        {{$student->email}}
                                    </td>
                                    <td>
                                        {{$student->phone}}
                                    </td>
                                    <td>
                                        <a href="?reg={{$student->id}}"
                                           class="btn btn-sm btn-outline-secondary">
                                            <i class="ni ni-camera-compact"></i>
                                            View Registrations
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                @else

                    <div class="card-body">
                        <p class="card-title text-center text-warning">No Data Available.</p>
                    </div>

                @endif

            </div>
        </div>

    </div>

    @if($stud)

        <!-- Modal -->
        <div class="modal fade" id="student-regs-modal" tabindex="-1" role="dialog" aria-labelledby="student-regs-modal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            Course Registrations: {{$stud->getFullName()}}
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        @if(isset($stud) && $stud->registrations->count() > 0)

                            <div class="table-responsive">
                                <!-- Projects table -->
                                <table class="table align-items-center table-flush">
                                    <thead class="thead-light">
                                    <tr>
                                        <th scope="col">Course</th>
                                        <th scope="col">Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($stud->registrations as $registration)
                                        <tr>
                                            <th scope="row">
                                                {{$registration->course->title}}
                                            </th>
                                            <td class="text-info font-weight-bold">
                                                {{$registration->passkey}}
                                            </td>
                                            <td>
                                                {{$registration->created_at->format('jS M, Y')}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        @else

                            <p class="card-title text-center text-warning">No Data Available.</p>

                        @endif

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


    @endif

@endsection


@push('scripts')

    @if(isset($stud) && $stud)
        <script>
            $('#student-regs-modal').modal('show');

            $('#student-regs-modal').on('hidden.bs.modal', function () {
                window.location.href = window.location.origin + window.location.pathname;
            })
        </script>
    @endif

@endpush