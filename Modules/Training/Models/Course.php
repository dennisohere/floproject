<?php

namespace Modules\Training\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\System\Traits\HasMetadataAttribute;
use Modules\Training\Repositories\StudentRepository;

class Course extends Model
{
    use HasMetadataAttribute;

    const CURRENCY_NGN = 0;
    const CURRENCY_GBP = 1;
    const CURRENCY_USD = 2;

    protected $table = 'courses';

    protected $guarded = ['id'];

    protected $dates = [
        'expires_at'
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function registrations()
    {
        return $this->hasMany(CourseRegistration::class,'course_id');
    }

    public function courseMaterials()
    {
        return $this->hasMany(CourseMaterial::class, 'course_id');
    }

    /**
     * @param $email
     * @param $passkey
     * @return CourseRegistration | null
     */
    public function findAndAuthenticateStudent($email, $passkey)
    {
        $student = StudentRepository::init()->getStudentByEmail($email);

        if(!$student) return null;

        return $this->registrations()
            ->where('student_id', $student->id)
            ->where('passkey', $passkey)
            ->first();
    }

    public function getCourseLink()
    {
        return route('training.course.details', ['course' => $this->slug]);
    }

    public function hasExpired()
    {
        return $this->expires_at ? now()->isAfter($this->expires_at) : false;
    }
}
