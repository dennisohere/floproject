<?php

namespace Modules\Training\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\System\Traits\HasMetadataAttribute;

class CourseMaterial extends Model
{
    use HasMetadataAttribute;

    protected $table = 'course_materials';

    protected $guarded = ['id'];

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
}
