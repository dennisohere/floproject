<?php

namespace Modules\Training\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Student extends Model
{
    use Notifiable;

    protected $guarded = ['id'];

    protected $table = 'students';

    public function registrations()
    {
        return $this->hasMany(CourseRegistration::class,'student_id');
    }

    public function getFullName()
    {
        return $this->last_name . ' ' . $this->first_name;
    }

    public function getRegistrationForCourse(Course $course)
    {
        return $this->registrations()->where('course_id', $course->id)->first();
    }
}
