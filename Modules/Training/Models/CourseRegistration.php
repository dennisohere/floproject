<?php

namespace Modules\Training\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\System\Traits\HasMetadataAttribute;

class CourseRegistration extends Model
{
    use HasMetadataAttribute;

    protected $table = 'course_registrations';

    protected $guarded = ['id'];

    public function students()
    {
        return $this->belongsTo(Student::class,'student_id');
    }

    public function course()
    {
        return $this->belongsTo(Course::class,'course_id');
    }
}
