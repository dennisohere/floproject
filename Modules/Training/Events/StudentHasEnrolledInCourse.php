<?php

namespace Modules\Training\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Modules\Training\Models\Course;
use Modules\Training\Models\Student;

class StudentHasEnrolledInCourse
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var Course
     */
    public $course;
    /**
     * @var Student
     */
    public $student;

    /**
     * Create a new event instance.
     *
     * @param Course $course
     * @param Student $student
     */
    public function __construct(Course $course, Student $student)
    {
        //
        $this->course = $course;
        $this->student = $student;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
