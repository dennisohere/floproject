<?php

namespace Modules\Training\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Modules\Training\Models\Course;
use Modules\Training\Models\CourseMaterial;
use Modules\Training\Repositories\CourseRepository;

class CoursesController extends Controller
{
    public function index()
    {
        $courses = CourseRepository::init()->getAllCourses();

        $edit = null;

        if($edit_slug = \request()->get('edit')){
            $edit = CourseRepository::init()->getCourseBySlug($edit_slug);
        }

        if($course_slug = \request()->get('course')){
            $course = CourseRepository::init()->getCourseBySlug($course_slug);
            $course->load('courseMaterials');

            if($course_material_id = \request()->get('material-edit')){
                $course_material_edit = $course->courseMaterials()->find($course_material_id);
        }
        }

        return view('training::backend.courses.index',
            compact('courses', 'edit', 'course', 'course_material_edit'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request)
    {
        $payload = $this->validate($request, [
            'id' => 'bail|sometimes|numeric',
            'title' => 'bail|required|string',
            'expiry' => 'bail|nullable',
            'cost' => 'bail|nullable|numeric',
//            'resource' => 'bail|required|string',
            'paystack_payment_url' => 'bail|nullable|string',
            'description' => 'bail|nullable|string',
        ]);

        CourseRepository::init()->saveCourse($payload);

        flash()->success('Saved!');

        return redirect()->back();
    }

    /**
     * @param Course $course
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function saveCourseModule(Course $course, Request $request)
    {
        $payload = $this->validate($request, [
            'id' => 'bail|sometimes|numeric',
            'title' => 'bail|required|string',
            'embed_code' => 'bail|required|string',
            'download_link' => 'bail|nullable|string',
        ]);

        $is_edit = !!isset($payload['id']);

        $course_material = $is_edit ?
            $course->courseMaterials()->where('id', $payload['id'])->first() :
            $course->courseMaterials()->newModelInstance();

        $course_material->fill([
           'title' => $payload['title'],
           'course_id' => $course->id,
           'video_embed_code' => $payload['embed_code'],
           'download_link' => $payload['download_link'],
        ]);

        $course_material->save();

        return redirect()->route('backend.training.courses.index', ['course' => $course->slug, 'modal' => 'module-list']);
    }

    /**
     * @param Course $course
     * @param CourseMaterial $material
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function deleteCourseModule(Course $course, CourseMaterial $material)
    {
        $material->delete();

        flash()->info('Deleted!');

        return redirect()->route('backend.training.courses.index', ['course' => $course->slug, 'modal' => 'module-list']);
    }
}
