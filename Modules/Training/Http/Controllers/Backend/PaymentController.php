<?php

namespace Modules\Training\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Modules\Training\Repositories\CourseRepository;
use Modules\Training\Repositories\PaystackRepository;

class PaymentController extends Controller
{
    public function paystack_webhook(Request $request)
    {
        $payload = $request->all();

        PaystackRepository::init()->logPayment($payload);

        if($payload['event'] !== 'charge.success') return ;

        // todo: check referrer
        $payment_referrer = $payload['data']['metadata']['referrer'] ?? null;

        $course = CourseRepository::init()->getCourseFromPaystackPaymentURL($payment_referrer);

        // no course found on this payment link. hence registration cannot proceed
        if(!$course) return ;

        try {
            $customer_payload = $payload['data']['customer'];
            CourseRepository::init()->registerNewStudentOnCourse($course, [
                'first_name' => $customer_payload['first_name'],
                'last_name' => $customer_payload['last_name'],
                'email' => $customer_payload['email'],
                'phone' => $customer_payload['phone'],
            ]);
        } catch (\Exception $exception){
            Log::error($exception->getMessage(), $exception->getTrace());
        }
    }
}
