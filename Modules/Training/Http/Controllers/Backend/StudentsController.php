<?php

namespace Modules\Training\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Modules\Training\Repositories\StudentRepository;

class StudentsController extends Controller
{
    public function index()
    {
        $students = StudentRepository::init()->getAllStudents();

        $edit = null;

        if($edit_id = \request()->get('edit')){
            $edit = StudentRepository::init()->getStudentById($edit_id);
        }

        $stud = null;
        if($student_id = \request()->get('reg')){
            $stud = StudentRepository::init()->getStudentById($student_id);
            $stud->load('registrations');
        }

        return view('training::backend.students.index', compact('students', 'edit', 'stud'));
    }
}
