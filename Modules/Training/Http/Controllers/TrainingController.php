<?php

namespace Modules\Training\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Modules\Training\Models\Course;
use Modules\Training\Models\CourseMaterial;

class TrainingController extends Controller
{

    public function details(Course $course)
    {
        return view('training::training.details', compact('course'));
    }

    public function authenticate(Course $course)
    {
        $payload = \request()->all();

        $email = $payload['email'];
        $passkey = $payload['passkey'];

        $registration = $course->findAndAuthenticateStudent($email, $passkey);

        if(!$registration && auth()->guest()){
            flash()->warning('Invalid credentials');
            return redirect()->route('training.course.details', ['course' => $course->slug]);
        }

        return view('training::training.start', compact('registration', 'course'));
    }

    public function watch_course_video(CourseMaterial $material)
    {
        return view('training::training.watch_video', compact('material'));
    }
}

