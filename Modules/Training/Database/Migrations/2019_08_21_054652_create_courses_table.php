<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('title');
            $table->string('slug',90)->unique();
            $table->string('code',9)->unique();
            $table->longText('description')->nullable();
            $table->dateTime('expires_at')->nullable();
            $table->double('cost')->default(0);

            $table->smallInteger('currency')->default(\Modules\Training\Models\Course::CURRENCY_NGN);

            $table->string('resource_link')->nullable();
            $table->string('paystack_payment_url',150)->nullable()->index();

            $table->longText('metadata')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
