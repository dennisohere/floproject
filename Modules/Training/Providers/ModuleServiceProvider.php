<?php

namespace Modules\Training\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('training', 'Resources/Lang', 'app'), 'training');
        $this->loadViewsFrom(module_path('training', 'Resources/Views', 'app'), 'training');
        $this->loadMigrationsFrom(module_path('training', 'Database/Migrations', 'app'), 'training');
        $this->loadConfigsFrom(module_path('training', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('training', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
