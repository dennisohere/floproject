<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 6/13/2019
 * Time: 10:23 AM
 */

namespace Modules\System\Traits;


use Illuminate\Support\Collection;

trait OrderNoTrait
{

    public function setOrderNumber($order_number, Collection $collection = null)
    {
        if($collection){
            $total_slots = $collection->where('order_no', '>=', $order_number)->pluck(['id', 'order_no']);
        } else {
            $total_slots = self::where('order_no', '>=', $order_number)->get(['id','order_no']);
        }


        foreach ($total_slots as $slot){
            self::where('id', $slot['id'])->update([
                'order_no' => $slot['order_no'] + 1
            ]);
        }

        $this->update([
            'order_no' => $order_number
        ]);

        return $this;
    }

}