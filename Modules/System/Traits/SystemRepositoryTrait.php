<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 8/15/2019
 * Time: 9:36 AM
 */

namespace Modules\System\Traits;


trait SystemRepositoryTrait
{
    /**
     * @return self
     */
    public static function init()
    {
        return app(self::class);
    }

}