<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 6/13/2019
 * Time: 10:23 AM
 */

namespace Modules\System\Traits;


trait HasAvatar
{

    public function getAvatar()
    {
        return $this->avatar ?? '/frontend/images/statelogo.png';
    }

}