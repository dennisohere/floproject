<?php

namespace Modules\Notifications\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('notifications', 'Resources/Lang', 'app'), 'notifications');
        $this->loadViewsFrom(module_path('notifications', 'Resources/Views', 'app'), 'notifications');
        $this->loadMigrationsFrom(module_path('notifications', 'Database/Migrations', 'app'), 'notifications');
        $this->loadConfigsFrom(module_path('notifications', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('notifications', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(EventServiceProvider::class);
        $this->app->register(RouteServiceProvider::class);
    }
}
