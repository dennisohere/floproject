<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 3/5/2019
 * Time: 12:32 AM
 */

namespace Modules\Notifications\EventSubscribers;



use App\Notifications\CourseRegistered;
use Illuminate\Support\Facades\Log;
use Modules\Training\Events\StudentHasEnrolledInCourse;
use Modules\Training\Repositories\MailerLiteRepository;

class StudentEventsSubscribers
{
    public function studentHasBeenEnrolledInCourse(StudentHasEnrolledInCourse $event)
    {
        $student = $event->student;

        MailerLiteRepository::init()->subscribeStudent($student);

        $student->notify(new CourseRegistered($event->course));

//        Log::info(StudentHasEnrolledInCourse::class, ['student' => $student, 'course' => $event->course]);
    }

    public function subscribe($events)
    {
        $events->listen(
            StudentHasEnrolledInCourse::class,
            self::class . '@studentHasBeenEnrolledInCourse'
        );
    }

}