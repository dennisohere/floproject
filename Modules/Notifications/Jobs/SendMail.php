<?php
/**
 * Created by PhpStorm.
 * User: denni
 * Date: 12/31/2018
 * Time: 4:57 PM
 */

namespace Modules\Notifications\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Notifications\Mailer\MailMan;

class SendMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    private $view;
    /**
     * @var array
     */
    private $data_for_view;
    private $email;
    private $subject;


    /**
     * SendMail constructor.
     * @param $view
     * @param array $data_for_view
     * @param $email
     * @param $subject
     */
    public function __construct($view, array $data_for_view, $email, $subject)
    {
        $this->view = $view;
        $this->data_for_view = $data_for_view;
        $this->email = $email;
        $this->subject = $subject;
    }

    public function handle()
    {
        try {
            MailMan::init()
                ->prepare($this->view, $this->data_for_view)
                ->send($this->email, $this->subject);
        } catch (\Throwable $e) {
        }
    }
}