<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 25/08/2017
 * Time: 10:38 AM
 */

namespace Modules\Notifications\Mailer;


use Illuminate\Support\Facades\Mail;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;

class MailMan
{
    /**
     * @var CssToInlineStyles
     */
    private $cssToInlineStyles;

    private $html;


    /**
     * HtmlMailer constructor.
     * @param CssToInlineStyles $cssToInlineStyles
     */
    public function __construct(CssToInlineStyles $cssToInlineStyles)
    {
        $this->cssToInlineStyles = $cssToInlineStyles;
    }

    /**
     * @param $htmlView
     * @param array $data
     * @param array $styles
     * @return $this
     * @throws \Throwable
     */
    public function prepare($htmlView, array $data = [], array $styles = [])
    {

        $html = view($htmlView)
            ->with($data)
            ->render();

//        $css = file_get_contents(public_path('/template/assets/plugins/bootstrap/css/bootstrap.min.css'));
        $css = file_get_contents(public_path('assets/backend/js/plugins/nucleo/css/nucleo.css'));
        $css .= file_get_contents(public_path('assets/backend/js/plugins/@fortawesome/fontawesome-free/css/all.min.css'));
        $css .= file_get_contents(public_path('assets/backend/css/argon-dashboard.css'));


        $this->html = $this->cssToInlineStyles->convert($html, $css);

        return $this;
    }

    public function send($email, $subject = '', $attachments = [])
    {
        \Mail::send('notifications::emails.layouts.custom-html-mail', ['custom_html' => $this->html],
            function ($message) use($email, $subject, $attachments){
            $message
                ->to($email)
                ->subject($subject)
            ;

            if(count($attachments) > 0){
                foreach ($attachments as $file) {
                    $message->attach($file); // attach each file
                }
            }
        });
    }

    /**
     * @return MailMan
     */
    public static function init()
    {
        return app(self::class);
    }
}